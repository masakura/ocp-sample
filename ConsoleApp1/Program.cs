﻿using System;
using MyLib;

namespace ConsoleApp1
{
    internal static class Program
    {
        private static void Main()
        {
            var source = new[] {1, 4, 21, 3, 6};
            var max = MyMath.Max(source);
            Console.WriteLine(max);
        }
    }
}