namespace MyLib
{
    public static class MyMath
    {
        public static int Max(int[] values)
        {
            var result = values[0];
            for (var i = 1; i < values.Length; i++)
            {
                var current = values[i];
                if (result < current) result = current;
            }

            return result;
        }

        public static int Min(int[] values)
        {
            var result = values[0];
            for (var i = 1; i < values.Length; i++)
            {
                var current = values[i];
                if (result > current) result = current;
            }

            return result;
        }
    }
}